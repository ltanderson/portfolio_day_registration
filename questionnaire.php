<?php 
session_start();
if ( !($_SESSION['validUser']==true) ) {
    header("location: logon.php");
    exit();
}
$successMsg='';

if ($_POST['qFormTracker']=="yes") {
    $_SESSION['userName'] = $_POST['inFName'];
    $_SESSION['userContact'] = $_POST['inContactEmail'];
    $_SESSION['userWebsite'] = $_POST['inWebsite'];
    $_SESSION['userQ1'] = $_POST['q1'];
    $_SESSION['userQ2'] = $_POST['q2'];

    $_SESSION['userMajor'] = $_POST['inMajor'];

    $student_full_name = htmlspecialchars($_POST['inFName'], ENT_QUOTES);
    $student_major = htmlspecialchars($_POST['inMajor'], ENT_QUOTES);
    $student_website = htmlspecialchars($_POST['inWebsite'], ENT_QUOTES);
    $student_contact_email = htmlspecialchars($_POST['inContactEmail'], ENT_QUOTES);
    $student_q1 = htmlspecialchars($_POST['q1'], ENT_QUOTES);
    $student_q2 = htmlspecialchars($_POST['q2'], ENT_QUOTES);

    $userEmail = $_SESSION["userEmail"];

    

    // ***change host=localhost;port=8889;dbname=portfolio_students ... , 'root', 'root'
    $dbh = new PDO('mysql:host=localhost;port=8889;dbname=portfolio_students;charset=utf8', 'root', 'root', array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

    $stmt = $dbh->prepare("UPDATE students_info SET student_full_name = ?, student_major = ?, student_website = ?, student_contact_email = ?, student_q1 = ?, student_q2 = ? WHERE student_email = ?");


    $stmt->bindValue(1, $student_full_name);
    $stmt->bindValue(2, $student_major);
    $stmt->bindValue(3, $student_website);
    $stmt->bindValue(4, $student_contact_email);
    $stmt->bindValue(5, $student_q1);
    $stmt->bindValue(6, $student_q2);
    $stmt->bindValue(7, $userEmail);
    $stmt->execute();

    $successMsg = "Changes saved. If you are satisfied with your answers, click logout at the top right.";


    
    /*$sql = "UPDATE students_info ";
    $sql .= "SET student_full_name = '$student_full_name', ";
    $sql .= "student_major = '$student_major', ";
    $sql .= "student_website = '$student_website', ";
    $sql .= "student_contact_email = '$student_contact_email', ";
    $sql .= "student_q1 = '$student_q1', ";
    $sql .= "student_q2 = '$student_q2' ";
    $sql .= "WHERE student_email='$userEmail'; ";

    include 'dbConnect.php';
    if( !mysqli_query($link, $sql) )
    {
        echo "The query failed. \n".mysqli_error($link) ;
    }
    else
    {
        echo "Worked";
    }*/

    $fullName = $student_full_name;
    $contactEmail = $student_contact_email;
    $website = $student_website;
    $q1 = $student_q1;
    $q2 = $student_q2;

    $selART = "";
    $selGRD = "";
    $selGRT = "";
    $selWDV = "";

    switch ($student_major) {
        case 'ART':
            $selART = "selected";
            break;

        case 'GRD':
            $selGRD = "selected";
            break;

        case 'GRT':
            $selGRT = "selected";
            break;

        case 'WDV':
            $selWDV = "selected";
            break;
        
        default:
            $selGRD = "selected";
            break;
    }
}
else
{
    $fullName = $_SESSION['userName'];
    if( $_SESSION['userContact']=="" )
    {
        $contactEmail = $_SESSION['userEmail'];
    }
    else
    {
        $contactEmail = $_SESSION['userContact'];
    }
    
    $website = $_SESSION['userWebsite'];
    $q1 = $_SESSION['userQ1'];
    $q2 = $_SESSION['userQ2'];
    $selART = "";
    $selGRD = "";
    $selGRT = "";
    $selWDV = "";

    switch ($_SESSION['userMajor']) {
        case 'ART':
            $selART = "selected";
            break;

        case 'GRD':
            $selGRD = "selected";
            break;

        case 'GRT':
            $selGRT = "selected";
            break;

        case 'WDV':
            $selWDV = "selected";
            break;
        
        default:
            $selGRD = "selected";
            break;
    }
}


?>
<html>
<head>
    <title>Portfolio Day Questionnaire</title>
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script type="text/javascript">
        var validForm;
        function requiredField(val)
        {
            if(val.trim()=="")
            {
                // nothing in the field
                validForm = false;
                return true;
            }
            else
            {
                return false;
            };
        }

        function qValidate()
        {
            validForm = true;
            var inFName = document.getElementById('inFName');
            var inCEmail = document.getElementById('inContactEmail');

            if(requiredField(inFName.value))
            {
                document.getElementById('inFNameErr').innerHTML = "Full name is required.";
            }
            else
            {
                document.getElementById('inFNameErr').innerHTML = "";
            };

            if (requiredField(inCEmail.value))
            {
                document.getElementById('inContactEmailErr').innerHTML = "Email is required.";
            }
            else
            {
                if( /^.+@\w+\..+$/i.test(inCEmail.value) )
                {
                    document.getElementById('inContactEmailErr').innerHTML = "";
                    inCEmail.style.borderColor = "initial";
                }
                else
                {
                    validForm = false;
                    inCEmail.style.borderColor = "red";
                    document.getElementById("inContactEmailErr").innerHTML = "Please use a valid email.";
                };
                
            };

            if(validForm)
            {
                document.getElementById('questForm').submit();
            };

        }
    </script>
    
</head>
<body>
    <div id="container">
        <div id="titleHeader">
              <img src="dmacclogo-white.png" />
        </div>
    
        <button id="logoutButton"><a href="logout.php">Logout</a></button>
    
    
        <div id="mainBody">


            <form name="questForm" id="questForm" method="post" action="questionnaire.php">           
            
                <div id="lineQs">
                    <label for="inFName" >Full Name:</label>
                    <input type="text" name="inFName" id="inFName" value="<?php echo $fullName; ?>" /><span id="inFNameErr"></span>
                    <br />


                    <label for="inMajor" >Major:</label>
                    <select name="inMajor" id="inMajor">
                        <option value="ART" <?php echo $selART; ?> >ART - Photography</option>
                        <option value="GRD" <?php echo $selGRD; ?> >GRD - Graphic Design</option>
                        <option value="GRT" <?php echo $selGRT; ?> >GRT - Graphic Tech</option>
                        <option value="WDV" <?php echo $selWDV; ?> >WDV - Web Development</option>
                    </select>
                    <span id="inMajorErr"></span>
                    <br />

                    <label for="inContactEmail" >Displayed Email:</label>
                    <input type="text" name="inContactEmail" id="inContactEmail" value="<?php echo $contactEmail; ?>" /><span id="inContactEmailErr"></span>
                    <br />

                    <label for="inWebsite" >Website Url: (optional)</label>
                    <input type="text" name="inWebsite" id="inWebsite" value="<?php echo $website; ?>" /><span id="inWebsiteErr"></span>
                    <br />
                </div>


                <div id="textQs">
                    What are your future career goals in your field of study?<br />
                    <textarea name="q1" id="q1" rows="10" cols="60" class="firstItem"><?php echo $q1; ?></textarea>
                    <br />
                    <br />

                    Please write a short, creative bio about yourself (see example).<br />
                    <textarea name="q2" id="q2" rows="10" cols="60" class="secondItem"><?php echo $q2; ?></textarea><span id="q2Example">I'm a graphic designer who communicates through bold colors, strong typography, and compelling concepts. By combining these things I create beautiful and meaningful work. I'm definitely not a stranger to late nights, and I'm always willing to put in the extra time to complete everything to the best it can be. On the weekends, I enjoy spending time at local antique stores looking for type ephemera, and catching up on my Hulu and Netflix queues.</span>
                    <br />
                    <br />
                </div><!-- end textQs -->

                <input type="hidden" name="qFormTracker" id="qFormTracker" value="yes" />

            </form>

            <button id="submitQs" onclick="qValidate()">Save Changes</button><span><?php echo $successMsg; ?></span>
            
        </div><!-- end mainBody -->
        
        

    </div><!-- end container -->
    
</body>
</html>