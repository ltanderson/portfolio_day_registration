<?php

if(isset($_POST['inEmail']))
{

    function generatePass($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    $inEmail = $_POST['inEmail'];
    $inPass = generatePass(6);

    include "dbConnect.php";

    $sql = "SELECT student_email, student_pass ";            
    $sql .= "FROM students_info "; 
    $sql .= "WHERE student_email='$inEmail'";

    if( ($tSelect = mysqli_query($link, $sql))  )
    {
        if(mysqli_num_rows($tSelect)==0)
        {
            // unused email
            $sql = "INSERT INTO students_info ";
            $sql .= "(student_email, student_pass) ";
            $sql .= "VALUES "; 
            $sql .= "('$inEmail', '$inPass')";
            mysqli_query($link, $sql);

            $eBody = "Thank you for registering. Here is your password for the questionnaire: $inPass. \n Use this with your DMACC email to logon here: http://leetanderson.com/portfolio_day/logon.php"; // ***change the url
            mail($inEmail, "Portfolio Day Registration", $eBody);

            $msg = "An email was sent to $inEmail with your password and the link to the portfolio day questionnaire.";
            
        }
        else
        {
            // pre-used email
            $userEmailPass = mysqli_fetch_array($tSelect);
            $inPass = $userEmailPass['student_pass'];
            $eBody = "Here is your password for the questionnaire: $inPass. \n Use this with your DMACC email to logon here: http://leetanderson.com/portfolio_day/logon.php"; // ***change the url
            mail($inEmail, "Portfolio Day Registration", $eBody);
            $msg = "An email was sent to $inEmail with your password and the link to the portfolio day questionnaire.";
        }
    }
    else
    {    
        echo "The query failed. \n".mysqli_error($link) ;
    }



}
mysqli_close($link);
?>
<html>
<head>
    <title>Register for Portfolio Day</title>
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script type="text/javascript">

        var validForm;
        

        function emailCompare()
        {
            if( document.getElementById('inEmail').value!=document.getElementById('validEmail').value )
            {
                document.getElementById("validEmailErr").innerHTML = "Email addresses must match.";
                document.getElementById('validEmail').style.borderColor = "red";
                
                validForm = false; 
            }
        }

        function eValid()
        {
            validForm = true;
            document.getElementById('inEmail').style.borderColor = "initial";
            document.getElementById("errMsg").innerHTML = "";
            document.getElementById("validEmailErr").innerHTML = "";
            document.getElementById('validEmail').style.borderColor = "initial";
            emailCompare();
            if( !/^.+@dmacc\.edu$/i.test(document.getElementById('inEmail').value) )
            {
                document.getElementById('inEmail').style.borderColor = "red";
                document.getElementById("errMsg").innerHTML = "Please use a DMACC email address.";
                validForm = false;
            }


            if(validForm) 
            {
                document.getElementById("registrationForm").submit();

            }
            
        }

    </script>
</head>
<body>
    <div id="container">
        <div id="titleHeader">
              <img src="dmacclogo-white.png" />
        </div>
    
    
        <div id="formContainer">
            <div id="mainBody">
                <p>Use your DMACC email to recieve your password to the portfolio day questionnaire.</p>
                <br />
                <form id="registrationForm" name="registrationForm" method="post" action="register.php">           
                
                
                <label for="inEmail" >Email:</label>
                <input type="email" name="inEmail" id="inEmail" /><br />
                <span id="inEmailErr"></span>

                <label for="validEmail" >Please retype your email:</label>
                <input type="email" name="validEmail" id="validEmail" /><br />
                <span id="validEmailErr"></span>
                

                </form>

                <button id="regSubmit" name="logSubmit" onclick="eValid()">Register</button>
                <span id="errMsg"><?php echo $msg; ?></span>
                
            </div><!-- end mainBody -->
        </div><!-- end formContainer -->


    </div><!-- end container -->
    
</body>
</html>

