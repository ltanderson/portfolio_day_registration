<?php
$errMsg = ""; 
if (isset($_POST['inEmail'])) {
    $inEmail = $_POST['inEmail'];
    $inPass = $_POST['inPass'];
    include 'dbConnect.php';


    $sql = "SELECT * ";            
    $sql .= "FROM students_info "; 
    $sql .= "WHERE student_email='$inEmail'";


    if( !($tSelect = mysqli_query($link, $sql))  )
    {
        echo "The query failed. \n".mysqli_error($link) ;
    }
            
    if(mysqli_num_rows($tSelect)==0)
    {
        // invalid username
        $errMsg = "Invalid username or password. Please try again.";
    }
    else
    {
        $row = mysqli_fetch_array($tSelect);

        if( $row['student_pass'] == $inPass )
        {
            session_start();
            $_SESSION['validUser'] = true;
            $_SESSION['userEmail'] = $row['student_email'];
            $_SESSION['userName'] = $row['student_full_name'];
            $_SESSION['userMajor'] = $row['student_major'];
            $_SESSION['userContact'] = $row['student_contact_email'];
            $_SESSION['userWebsite'] = $row['student_website'];
            $_SESSION['userQ1'] = $row['student_q1'];
            $_SESSION['userQ2'] = $row['student_q2'];

            header("location: questionnaire.php");
            mysqli_close($link);
            exit(); 
        }
        else
        {
            // invalid password
            $errMsg = "Invalid username or password. Please try again.";
        }
    }
}


?>
<html>
<head>
    <title>Portfolio Day Logon</title>
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <script type="text/javascript">
        function logValidate()
        {
            var inEmail = document.getElementById('inEmail');
            inEmail.style.borderColor = "initial";
            document.getElementById("inEmailErr").innerHTML = "";
            var inPass = document.getElementById('inPass');
            inPass.style.borderColor = "initial";
            document.getElementById("inPassErr").innerHTML = "";
            var validForm = true;
            if( !(/^.+@dmacc\.edu$/i.test(inEmail.value)) )
            {
                validForm = false;
                inEmail.style.borderColor = "red";
                document.getElementById("inEmailErr").innerHTML = "Please use your DMACC email.";
            };

            if( !(/^\w{6}$/i.test(inPass.value)) )
            {
                validForm = false;
                inPass.style.borderColor = "red";
                document.getElementById("inPassErr").innerHTML = "Password must be six characters long.";
            };


            if(validForm)
            {
                document.getElementById("logonForm").submit();
            };
        }
    </script>
</head>
<body>
    <div id="container">
        <div id="titleHeader">
              <img src="dmacclogo-white.png" />
        </div><!-- end titleHeader -->
    
        <div id="formContainer">
            <div id="mainbody">
                <p>To continue to the questionnaire, please logon with your DMACC email and the password that was sent to you.</p>
                <form id="logonForm" name="logonForm" method="post" action="logon.php">           
                
                    <label for="inEmail" >Email:</label>
                    <input type="text" name="inEmail" id="inEmail" /><br />
                    <span id="inEmailErr"></span>
                    <br />

                    <label for="inPass" >Password:</label>
                    <input type="password" name="inPass" id="inPass" /><br />
                    <span id="inPassErr"></span>
                    <br />

                </form>

                <button id="logSubmit" name="logSubmit" onclick="logValidate()">Logon</button>
                <span id="errMsg"><?php echo $errMsg; ?></span>
            </div><!-- end mainBody -->
        </div><!-- end formContainer -->
        

    </div><!-- end container -->
    
</body>
</html>